## 0.3.0 (2022-06-15)

### New features (1 change)

- [Add self-updater](lordgaav/sops-batch@965bfc79e3edfd54b4695f89b6a3827c99d3e11d)

### Feature changes (1 change)

- [Reduced filesize of binary](lordgaav/sops-batch@b8670c4c2f06f78ba163b5f3f1262b57b8d9d15b)

## 0.2.1 (2022-06-13)

### Feature changes (1 change)

- [Only show shell completion generation message when verbose is enabled](lordgaav/sops-batch@5ea5a2809501979280979bb917756c3c47bdddc8)

### CI improvements (1 change)

- [Switch CI build process to cargo-make.](lordgaav/sops-batch@09535f2aa37ae3cbcb63726eebf310bc5e62309b)

## 0.2.0 (2022-06-12)

### New features (2 changes)

- [Add better usage examples](lordgaav/sops-batch@3b4e7f1ee1c58a164bfa84b6274fc4f724f5a618)
- [Implement update-keys](lordgaav/sops-batch@af21b8220290fc6f6c5f1b65a86babee91b6fc06)

## v0.1.0 (2022-06-09)

Initial release.

### New features (4 changes)

- Added encrypt feature
- Added decrypt feature
- Added updatekeys feature
- Added shell completions

### CI improvements (1 change)

- [Added Gitlab CI configuration](lordgaav/sops-batch@65ab80f6c9c3d29051b0c4a3ff9ca7ff17346635)
