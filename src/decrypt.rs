use crate::common::{call_sops, load_files_config, FilePair};
use std::path::Path;

pub fn decrypt(
    files_config_path: &Path,
    sops_bin: &Path,
    allow_decryption_fail: bool,
    verbose: bool,
) -> Result<(), String> {
    let files_config = match load_files_config(files_config_path) {
        Ok(c) => c,
        Err(e) => return Err(e),
    };
    for filename in files_config.files {
        let file_pair = FilePair::from_filename(filename.as_str());
        let args: Vec<&str> = [
            "--decrypt",
            "--output",
            file_pair.dec_name.as_str(),
            file_pair.enc_name.as_str(),
        ]
        .to_vec();
        eprintln!(
            "decrypting {} to {}",
            file_pair.enc_name, file_pair.dec_name
        );
        let status = match call_sops(sops_bin, args, verbose) {
            Ok(c) => c,
            Err(err) => return Err(format!("failed to decrypt {}: {}", file_pair.enc_name, err)),
        };
        let status_code = match status.code() {
            Some(c) => c,
            None => return Err("process terminated".to_string()),
        };
        match status_code {
            0 => continue,
            128 => {
                if allow_decryption_fail {
                    eprintln!(
                        "decryption of {} failed, but was allowed, skipping...",
                        file_pair.enc_name
                    );
                    continue;
                } else {
                    return Err(format!(
                        "failed to decrypt {}: decryption failed but not allowed",
                        file_pair.enc_name
                    ));
                }
            }
            other => {
                return Err(format!(
                    "failed to decrypt {}: sops exit code {}",
                    file_pair.enc_name, other
                ));
            }
        };
    }
    Ok(())
}
