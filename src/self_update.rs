use self_update::cargo_crate_version;
use self_update::Status;

pub fn self_update() -> Result<String, self_update::errors::Error> {
    let status = self_update::backends::gitlab::Update::configure()
        .repo_owner("lordgaav")
        .repo_name("sops-batch")
        .bin_name("sops-batch")
        .show_download_progress(true)
        .current_version(cargo_crate_version!())
        .build()?
        .update()?;
    let msg = match status {
        Status::UpToDate(s) => format!("Already at the latest version: {}.", s),
        Status::Updated(s) => format!("Binary updated to version {}.", s),
    };
    Ok(msg)
}
