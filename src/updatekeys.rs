use crate::common::{call_sops, load_files_config, FilePair};
use std::path::Path;

pub fn update_keys(files_config_path: &Path, sops_bin: &Path, verbose: bool) -> Result<(), String> {
    let files_config = match load_files_config(files_config_path) {
        Ok(c) => c,
        Err(e) => return Err(e),
    };
    for filename in files_config.files {
        let file_pair = FilePair::from_filename(filename.as_str());
        let args: Vec<&str> = ["updatekeys", file_pair.enc_name.as_str()].to_vec();
        eprintln!("updating keys on {}", file_pair.enc_name);
        let status = match call_sops(sops_bin, args, verbose) {
            Ok(c) => c,
            Err(err) => {
                return Err(format!(
                    "failed to update keys on {}: {}",
                    file_pair.enc_name, err
                ))
            }
        };
        let status_code = match status.code() {
            Some(c) => c,
            None => return Err("process terminated".to_string()),
        };
        match status_code {
            0 => continue,
            other => {
                return Err(format!(
                    "failed to update keys on {}: sops exit code {}",
                    file_pair.enc_name, other
                ));
            }
        };
    }
    Ok(())
}
