#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

usage() {
    echo "usage: $0 NEWVERSION";
    exit 1;
}

if [ $# -lt 1 ]; then
    usage
fi

VERSION=$1

set -x

echo "Set Cargo version to $VERSION"
sed -i 's/^version = ".*"/version = "'${VERSION}'"/' Cargo.toml
cargo update -w

echo "Generate README"
cargo readme > README.md
