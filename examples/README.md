# Usage example

This folder contains:

- `.sops.yaml`: sops configuration to configure what keys to use for decrypt and update-keys.
- `.sops-batch.toml`: sops-batch configuration to configure what files to operate on.
- `key.txt`: example age key.
- `foo.yaml`, `bar.json`: example files to operate on.

[[_TOC_]]

## Decryption

The example folder contains encrypted example files

```sh
# We need to set SOPS_AGE_KEY_FILE to tell sops to look for our key.txt
$ export SOPS_AGE_KEY_FILE=key.txt
```

```sh
# Decrypt the files
$ sops-batch decrypt -f .sops-batch.toml
decrypting foo.enc.yaml to foo.yaml
decrypting bar.enc.json to bar.json
```

## Encryption

Normally you'll want to use `sops <filename>` to modify an encrypted file, and call `sops-batch decrypt` afterwards. Changing the decrypted file and calling `sops-batch encrypt` afterwards also works, but will generate new keys and clutter your Git history.

### Edit encrypted file using sops

```sh
# We need to set SOPS_AGE_KEY_FILE to tell sops to look for our key.txt
$ export SOPS_AGE_KEY_FILE=key.txt
```

```sh
# Edit a single file using sops, opens an editor
$ sops foo.enc.yaml
```

```sh
# Call sops-batch decrypt to update the decrypted version
$ sops-batch decrypt -f .sops-batch.toml
```

```sh
# The git history is clean and concise:
$ git diff foo.enc.yaml
```
```diff
diff --git a/examples/foo.enc.yaml b/examples/foo.enc.yaml
index 55d0b98..1ddbf1c 100644
--- a/examples/foo.enc.yaml
+++ b/examples/foo.enc.yaml
@@ -4,6 +4,7 @@ foo:
             - ENC[AES256_GCM,data:5uvN,iv:GJRLwHtllqTLqqowGOHwpIppaZDcv0RilmwthEpZoI4=,tag:ufvjFxDc1CykgU9AdpdBVQ==,type:str]
             - ENC[AES256_GCM,data:Fusc,iv:UEB2TYZpAesi41zt2aCrl16E3hp89PhbUDQvHY0Ifws=,tag:278N5qHkJi0UooFYM3xBGw==,type:str]
             - ENC[AES256_GCM,data:tjUgq0c=,iv:wqXJkaX8O/lj1OSgC2VIuZvv/cHXG2gz1jEh0iaiFyo=,tag:1ZAIYEYd4XiBGZt7jjoL2A==,type:str]
+pi: ENC[AES256_GCM,data:Jlw8IQ==,iv:AYl39gmCoQQI3ggZALMW+45YVgEbUIIRBaxtWyUn+pk=,tag:NAUYOpcwG1vMLJQN2L3Muw==,type:float]
 sops:
     kms: []
     gcp_kms: []
@@ -19,8 +20,8 @@ sops:
             K0VpenNVWXlWTjJEZXZQb1hOV3dJRE0KjFd/dGGTnoTI+Fh/f+FxCos3wEsKFUEb
             7dPzETg/h/oapVPi+HL4+or7SlCKCJ+2h8FVYhHtqkSqtZ8Oq+dofA==
             -----END AGE ENCRYPTED FILE-----
-    lastmodified: "2022-06-12T11:42:15Z"
-    mac: ENC[AES256_GCM,data:Xp1vc8DFrB1pNf9Ko35AsCdj7vjQCzuTTlvHthSS3wKD26vOtu95S++E1r9pEQGmcLPPQeKux5w9m34hem4qD4l8dOTq0Y39rQng4Aeokpie91nhdhn16d7ylvuMUpavRnTe675AbKOfGSEj+6PVaXkX7KpXmnYSyLwDaqEy9MY=,iv:rS2IQheCIckPe8LQlZSWxUcLMVRLgNNzpS54X5d9HPQ=,tag:2WI7Z+leE9QNGXCOmUCapw==,type:str]
+    lastmodified: "2022-06-12T11:48:24Z"
+    mac: ENC[AES256_GCM,data:e05VFuOJpbm56vKHvr+gp0xfiF7ueCwaS0jks7RaGZu2buDdAd9Zob/Ut9vDj0RIGjhAcTlqrYtvkPQZu4MbVRKj8sUbZicx2uuXGpb10u8EgaA4hhqKiKvItXKVsWkPeKVTh26E0rn748pvjaOpnHOKt2AAdcmes09oNb3ik7Y=,iv:XzTe5nmmYGwbTIMLu1zyvbRvKgMCXbIw0HlvIpHeH7c=,tag:KQMigY7eADhmgUmtZXzemQ==,type:str]
     pgp: []
     unencrypted_suffix: _unencrypted
     version: 3.7.3
```

### (Re-)encrypt decrypted files in bulk

```sh
# Edit the decrypted file
$ vim foo.yaml
```

```sh
# Call sops-batch
$ sops-batch encrypt -f .sops-batch.toml
encrypting foo.yaml to foo.enc.yaml
encrypting bar.json to bar.enc.json
```

```sh
# Note that the git history shows that all keys have been re-encrypted and a new key has been generated, for all files
$ git diff foo.enc.yaml
```

```diff
diff --git a/examples/foo.enc.yaml b/examples/foo.enc.yaml
index 55d0b98..a88ff5d 100644
--- a/examples/foo.enc.yaml
+++ b/examples/foo.enc.yaml
@@ -1,9 +1,10 @@
 foo:
     bar:
         baz:
-            - ENC[AES256_GCM,data:5uvN,iv:GJRLwHtllqTLqqowGOHwpIppaZDcv0RilmwthEpZoI4=,tag:ufvjFxDc1CykgU9AdpdBVQ==,type:str]
-            - ENC[AES256_GCM,data:Fusc,iv:UEB2TYZpAesi41zt2aCrl16E3hp89PhbUDQvHY0Ifws=,tag:278N5qHkJi0UooFYM3xBGw==,type:str]
-            - ENC[AES256_GCM,data:tjUgq0c=,iv:wqXJkaX8O/lj1OSgC2VIuZvv/cHXG2gz1jEh0iaiFyo=,tag:1ZAIYEYd4XiBGZt7jjoL2A==,type:str]
+            - ENC[AES256_GCM,data:Q/QM,iv:WwsJX7y8qDyAq4RFhc/MPbdCagz9ngDOVp1YDp7Yp/o=,tag:vlv+lh7yy84QTZqpz9aadQ==,type:str]
+            - ENC[AES256_GCM,data:wrY6,iv:ikiQ/rB83QZBuxIbJRsbDmEOroEzSdRoYUicME9SK6o=,tag:HEbKBg6VacyuMteuydZGWw==,type:str]
+            - ENC[AES256_GCM,data:ag+5PR4=,iv:MOCukKMcOns6EoELAd4BbAo+NkiwkOe11/C8X5ok7jk=,tag:aDilynWMWjqLdevcLQwMZg==,type:str]
+pi: ENC[AES256_GCM,data:vk5+7w==,iv:H5MJt/PlVdd6Cw/swOsPm+JwbCYYQ3xWUAx+h0HeFJY=,tag:u+pbgTJLRQ1cMf7iIdVVuQ==,type:float]
 sops:
     kms: []
     gcp_kms: []
@@ -13,14 +14,14 @@ sops:
         - recipient: age1ps5g8fcm64358f7gkvxplqm6wavthvh0lxdtfzzdm8tvr2ate36sxncq0m
           enc: |
             -----BEGIN AGE ENCRYPTED FILE-----
-            YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSBET3ZhUy9YVXFjOXJXVTht
-            THNUVmFTUWRpS05QTkZQZWZaWUYyYWcwWVEwCmR5SFVNaG50bVdPeXB0TnNTUkJs
-            RGYvZVY5Z1pCZm5GRVVDa2J2N1cyUkkKLS0tIFlwRWVjUGJkZ0dYNi9La2pmc0tC
-            K0VpenNVWXlWTjJEZXZQb1hOV3dJRE0KjFd/dGGTnoTI+Fh/f+FxCos3wEsKFUEb
-            7dPzETg/h/oapVPi+HL4+or7SlCKCJ+2h8FVYhHtqkSqtZ8Oq+dofA==
+            YWdlLWVuY3J5cHRpb24ub3JnL3YxCi0+IFgyNTUxOSAwR1RuMFRSQmVJNHpZcklq
+            MTY4WmlsMVg5anl4anlQMFg5VXZoajFIOUZZClg5bk5ncVpxMUJUSGR3aFU4MEU0
+            MC9ScXEwSHBaMnZvaXpySlQ4dmw3akkKLS0tIGFsaWxraFM2NFIxWGFnUTM1QlN0
+            bmRRZWJQZ3hMeGszTTM3NmlvOW5WaUkKEIcG+ex/reNy44cFXY+uPkoNMLUllK81
+            cWB/OiPfpbbQCpttQCXP4Fx9ORqI33ytaeKNpIqjV8UJcPfNUnGB1w==
             -----END AGE ENCRYPTED FILE-----
-    lastmodified: "2022-06-12T11:42:15Z"
-    mac: ENC[AES256_GCM,data:Xp1vc8DFrB1pNf9Ko35AsCdj7vjQCzuTTlvHthSS3wKD26vOtu95S++E1r9pEQGmcLPPQeKux5w9m34hem4qD4l8dOTq0Y39rQng4Aeokpie91nhdhn16d7ylvuMUpavRnTe675AbKOfGSEj+6PVaXkX7KpXmnYSyLwDaqEy9MY=,iv:rS2IQheCIckPe8LQlZSWxUcLMVRLgNNzpS54X5d9HPQ=,tag:2WI7Z+leE9QNGXCOmUCapw==,type:str]
+    lastmodified: "2022-06-12T11:51:59Z"
+    mac: ENC[AES256_GCM,data:f8P7x35RPmVKVxBY7pIoA0TOAgRhgTq2F16hLlTf1hJgfkDb/dSW4NwDWjgiUtItHmTxdZCwU06lA4SHLoBG1CK50DaJYBcujkcE/g4ViGAZTdKuhr5WNw53sQCLXpCL3IB3IIX5zXDqUTM/DM1PQ11Por+KqfsJbtSV4Am7uig=,iv:mTT79CkIjL+2lmU+3YLgiZ/4iOvVyK1d8pleN9GgZe0=,tag:2+Pzvm4ggqkwXSN7bDZ/ew==,type:str]
     pgp: []
     unencrypted_suffix: _unencrypted
     version: 3.7.3
```

## Update keys on all encrypted files

When adding a new key to `.sops.yaml` (or changing keygroups), call `sops-batch update-keys` to update all files to match your encryption config.

```sh
# We need to set SOPS_AGE_KEY_FILE to tell sops to look for our key.txt
$ export SOPS_AGE_KEY_FILE=key.txt
```

```sh
# Edit .sops.yaml
$ vim .sops.yaml
$ git diff .sops.yaml
```

```diff
diff --git a/examples/.sops.yaml b/examples/.sops.yaml
index 5c2265c..4c638a0 100644
--- a/examples/.sops.yaml
+++ b/examples/.sops.yaml
@@ -2,3 +2,5 @@ creation_rules:
 - key_groups:
   - age:
     - "age1ps5g8fcm64358f7gkvxplqm6wavthvh0lxdtfzzdm8tvr2ate36sxncq0m"
+  - pgp:
+    - "E30F3470BB5002FA5BB13D403D5B72B1CD9C0E84"
```

```sh
# Update keys, note that this command is interactive
$ sops-batch update-keys -f .sops-batch.yaml
updating keys on foo.enc.yaml
2022/06/12 13:57:18 Syncing keys for file /home/lordgaav/workspace/sops-batch/examples/foo.enc.yaml
The following changes will be made to the file's groups:
Group 1
    age1ps5g8fcm64358f7gkvxplqm6wavthvh0lxdtfzzdm8tvr2ate36sxncq0m
Group 2
+++ E30F3470BB5002FA5BB13D403D5B72B1CD9C0E84
Is this okay? (y/n):
...
```
