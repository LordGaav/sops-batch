# sops-batch

Current version: 0.4.0

SOPS encryption / decryption batch tool

A wrapper around sops written in Rust, that allows bulk encryption, decryption or updating of keys, based on a configuration file.

## Configuration / Installation
1. Create a `.sops.yaml` file that specifies what keys to use:
```yaml
creation_rules:
  - key_groups:
      - pgp:
          - "XXXXX"
      - age:
          - "XXXXX"
```
2. Create a `.sops-batch.toml` file that specifies what files to act on:
```toml
files = [
  "foo.yaml",
  "bar.json"
]
```
3. [Download](https://gitlab.com/LordGaav/sops-batch/-/releases) and place `sops-batch` in your `$PATH`.
    * Linux users download the `*-linux-gnu` version, or the `*-linux-musl` version for a static binary.
    * MacOS users download the `*-apple-darwin` version. This should also work on M1.
    * Windows users download the `*-windows-gnu` version.
4. Use it: `sops-batch -h`.

## Examples

See the [examples folder](examples/README.md).

## Update

`sops-batch` includes a self-update feature, introduced in version `0.3.0`:

```sh
$ sops-batch self-update
Checking target-arch... x86_64-unknown-linux-gnu
Checking current version... v0.0.0
Checking latest released version... v0.3.0
New release found! v0.0.0 --> v0.3.0
New release is *NOT* compatible

sops-batch release status:
  * Current exe: ".../sops-batch"
  * New exe release: "sops-batch_0.3.0_x86_64-unknown-linux-gnu"
  * New exe download url: "https://gitlab.com/api/v4/projects/36884529/packages/generic/0.3.0/x86_64-unknown-linux-gnu/sops-batch"

The new release will be downloaded/extracted and the existing binary will be replaced.
Do you want to continue? [Y/n] y
Downloading...
[00:00:00] [========================================] 4.82MiB/4.82MiB (0s) Done
Extracting archive... Done
Replacing binary file... Done
Binary updated to version 0.3.0.
```

# License

MPL-2.0, see LICENSE file for details.
